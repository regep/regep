# Reflection-based Genetic Programming (ReGeP)

ReGeP is a software library intended as an extension to GP frameworks, and its main purpose is the construction of the primitive set of a GP system by providing

* a wrapping layer which allows decoupling of functions and terminals from the GP system in which they are used
* an infrastructure which offers gradual processing of elements--starting from package or class names and ending with GP function ready for use
* validation of the GP function set so that no unusable functions clutter the primitive set.

Reflection is used to automatically discover functions usable in a GP system, and there is no need to adjust or re-implement functions before using them.

ReGeP is not a standalone application, and was first developed for use with the [Jpea](https://gitlab.com/jpea/jpea) application.
