package de.saviola.regep.core;

import de.saviola.regep.core.processing.interfaces.Processor;

/**
 * This interface is implemented by classes which serve a purpose only for
 * specific objects (or, e.g., classes).
 * 
 * Two examples of such classes are classes implementing {@link Processor} and
 * {@link Filter}.
 */
public interface Applicable
{
  /**
   * Returns true if the the object upon which this method is called is
   * applicable for the given object.
   */
  public boolean isApplicableFor(Object input);
}
