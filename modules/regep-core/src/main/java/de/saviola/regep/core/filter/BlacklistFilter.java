package de.saviola.regep.core.filter;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import de.saviola.regep.core.filter.interfaces.Filter;

/**
 * Filter accepting everything that is not on its blacklist.
 */
public class BlacklistFilter<T> implements Filter
{
  /**
   * Blacklist of this filter.
   */
  Set<T> blacklist;

  /**
   * Initializes this filter's blacklist with the given objects.
   */
  public BlacklistFilter(final T[] objects)
  {
    this.blacklist = new HashSet<>();
    this.blacklist.addAll(Arrays.asList(objects));
  }

  /**
   * Returns false if the given object is in the blacklist, true otherwise.
   */
  @Override
  public boolean accept(final Object o)
  {
    return !this.blacklist.contains(o);
  }

  /**
   * Adds an entry to the blacklist.
   */
  public void addEntry(final T entry)
  {
    this.blacklist.add(entry);
  }

  /**
   * Returns the blacklist of this filter.
   */
  public Set<T> getBlacklist()
  {
    return this.blacklist;
  }

  /**
   * The blacklist filter is applicable for all objects, return true.
   */
  @Override
  public boolean isApplicableFor(final Object input)
  {
    return true;
  }

  /**
   * Replaces the blacklist of this filter with the given set of objects.
   */
  public void setBlacklist(final Set<T> blacklist)
  {
    this.blacklist = blacklist;
  }
}