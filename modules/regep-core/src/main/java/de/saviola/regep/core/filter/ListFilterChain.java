package de.saviola.regep.core.filter;

import java.util.ArrayList;
import java.util.List;

import de.saviola.regep.core.filter.interfaces.Filter;
import de.saviola.regep.core.filter.interfaces.FilterChain;

/**
 * A list-based iterative filter chain implementation. 
 */
public class ListFilterChain implements FilterChain
{
  /**
   * Filter list.
   */
  List<Filter> filters = new ArrayList<>();

  /**
   * Returns true if all filters accepted the given object.
   */
  @Override
  public boolean accept(final Object o)
  {
    // Walk through all filters, aborting as soon as one does not accept o
    for (final Filter filter : this.filters)
    {
      if (!filter.accept(o))
      {
        return false;
      }
    }

    // All filters in the chain have accepted o
    return true;
  }

  /**
   * Adds the given filter to the chain.
   */
  @Override
  public void addFilter(final Filter filter)
  {
    // TODO: throw exception?
    // This would be bad, as it would create a closed chain
    if (filter == this)
    {
      return;
    }

    this.filters.add(filter);
  }

  /**
   * Returns true if at least one filter is applicable for the given object.
   */
  @Override
  public boolean isApplicableFor(final Object o)
  {
    // Walk through all filters, aborting as soon as one is applicable for the
    // given object
    for (final Filter filter : this.filters)
    {
      if (filter.isApplicableFor(o))
      {
        return true;
      }
    }

    // There is no filter applicable for the given object
    return false;
  }

}
