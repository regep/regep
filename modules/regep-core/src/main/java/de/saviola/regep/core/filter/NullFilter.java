package de.saviola.regep.core.filter;

import de.saviola.regep.core.filter.interfaces.Filter;

/**
 * Null filter. Accepts everything and is applicable for nothing.
 */
public class NullFilter implements Filter
{
  @Override
  public boolean isApplicableFor(Object input)
  {
    return false;
  }

  @Override
  public boolean accept(Object o)
  {
    return true;
  }

}
