package de.saviola.regep.core.filter.interfaces;

import de.saviola.regep.core.Applicable;

/**
 * Universal filtering interface.
 */
public interface Filter extends Applicable
{
  /**
   * Returns true if the object is accepted by the filter, false otherwise.
   */
  public boolean accept(Object o);
}
