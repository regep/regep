package de.saviola.regep.core.filter.interfaces;

/**
 * Interface to allow filter chaining.
 */
public interface FilterChain extends Filter
{
  /**
   * Adds a filter to the chain.
   */
  public void addFilter(Filter filter);
}
