package de.saviola.regep.core.filter.interfaces;

/**
 * Interface providing filter support.
 */
public interface FilterSupport
{
  /**
   * Returns the filter registered with the class or null if none is registered
   */
  public Filter getFilter();

  /**
   * Sets the filter.
   */
  public void setFilter(Filter filter);
}
