package de.saviola.regep.core.gp.function;

import de.saviola.regep.core.gp.function.interfaces.Function;

/**
 * Abstract superclass of all GP-function classes.
 * 
 * TODO: Do we need this class?
 */
public abstract class AbstractFunction implements Function
{

}
