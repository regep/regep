package de.saviola.regep.core.gp.function;

import com.google.common.primitives.Primitives;

import de.saviola.regep.core.gp.function.interfaces.CachingFunction;
import de.saviola.regep.core.gp.function.interfaces.Function;

/**
 * Function wrapper that implements a caching mechanism.
 * 
 * Most calls to the underlying function are cached. Additionally, it
 * replaces classes of primitives types with their wrapper classes.
 */
public class CachingFunctionWrapper implements CachingFunction
{
  /**
   * Cached arity of the function (return value of {@link #getArity()}.
   * 
   * Will be cached upon creation, is not changeable afterwards.
   */
  private int cachedArity;

  /**
   * Cached child types of the function (return value of
   * {@link #getChildType(int)}.
   * 
   * Will be cached upon creation, is not changeable afterwards.
   */
  private Class<?>[] cachedChildTypes;

  /**
   * Cached return type of the function (return value of
   * {@link #getReturnType()}.
   * 
   * Will be cached upon creation, is not changeable afterwards.
   */
  private Class<?> cachedReturnType;

  /**
   * The cached value (return value of {@link #execute(Object[])}).
   * 
   * Can be (re)set via {@link #setCache(Object)} and {@link #resetCache()}.
   */
  private Object cachedValue;

  /**
   * The wrapped function.
   */
  private final Function function;

  /**
   * Creates a new caching function wrapper.
   */
  public CachingFunctionWrapper(final Function function)
  {
    this.cachedValue = null;
    this.function = function;

    this.fillCache();
  }

  /**
   * Executes the underlying function or return the cached value.
   * 
   * Executes the underlying function only if there's no cached value or the
   * arity of the function is not 0 (i.e. it's not a terminal).
   */
  @Override
  public Object execute(final Object[] args) throws Exception
  {
    // If this is not a terminal, no caching is enabled
    if (this.cachedArity > 0)
    {
      return this.function.execute(args);
    }

    // If there's no cached value, execute it
    if (this.cachedValue == null)
    {
      this.executeAndCache(args);
    }

    return this.cachedValue;
  }

  /**
   * Executes and caches the function if no cached value exists.
   * 
   * Otherwise nothing is done.
   */
  private synchronized void executeAndCache(final Object[] args)
    throws Exception
  {
    if (this.cachedValue == null)
    {
      this.cachedValue = this.function.execute(args);
    }
  }

  /**
   * Fills the cache with values which will never change during the lifetime
   * of this function wrapper.
   */
  private void fillCache()
  {
    this.cachedArity = this.function.getArity();
    this.cachedReturnType = Primitives.wrap(this.function.getReturnType());

    this.cachedChildTypes = new Class<?>[this.cachedArity];

    for (int i = 0; i < this.cachedArity; i++)
    {
      this.cachedChildTypes[i] = Primitives.wrap(this.function.getChildType(i));
    }
  }

  /**
   * Returns the cached arity of the function.
   */
  @Override
  public int getArity()
  {
    return this.cachedArity;
  }

  /**
   * Returns the cached child type of the function.
   */
  @Override
  public Class<?> getChildType(final int childNumber)
  {
    return this.cachedChildTypes[childNumber];
  }

  public Function getFunction()
  {
    return this.function;
  }

  /**
   * Returns the cached return type of the function.
   */
  @Override
  public Class<?> getReturnType()
  {
    return this.cachedReturnType;
  }

  /**
   * Resets the cached return value of {@link #execute(Object[])}.
   */
  @Override
  public void resetCache()
  {
    this.cachedValue = null;
  }

  /**
   * Sets the cached return value of {@link #execute(Object[])}.
   */
  @Override
  public void setCache(final Object value)
  {
    this.cachedValue = value;
  }

  /**
   * Returns the string representation of the underlying function (i.e.
   * {@link Function#toString()}) and appends the cached value of
   * {@link #execute(Object[])} to it, if the function is a terminal.
   */
  @Override
  public String toString()
  {
    return this.function.toString()
      + (this.getArity() == 0 ? "[" + this.cachedValue + "]" : "");
  }
}
