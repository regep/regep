package de.saviola.regep.core.gp.function;

import java.lang.reflect.Constructor;

/**
 * GP-function which wraps a constructor.
 * 
 * TODO rename?
 */
public class ConstructorFunction extends AbstractFunction
{
  /**
   * The wrapped constructor.
   */
  private Constructor<?> constructor;

  public ConstructorFunction(Constructor<?> constructor)
  {
    this.constructor = constructor;
  }

  /**
   * Executes the GP-function, i.e. creates a new instance of the class which
   * declares the wrapped constructor.
   */
  @Override
  public Object execute(Object[] args) throws Exception
  {
    return this.constructor.newInstance(args);
  }

  /**
   * Returns the number of arguments the constructor expects.
   */
  @Override
  public int getArity()
  {
    return this.constructor.getParameterTypes().length;
  }

  /**
   * Returns the type of the constructor argument with the given number.
   */
  @Override
  public Class<?> getChildType(int childNumber)
  {
    return this.constructor.getParameterTypes()[childNumber];
  }

  /**
   * Returns the class which declares the constructor. 
   */
  @Override
  public Class<?> getReturnType()
  {
    return this.constructor.getDeclaringClass();
  }

  /**
   * Returns {@link Constructor#toString()}.
   */
  @Override
  public String toString()
  {
    return this.constructor.toString();
  }
}
