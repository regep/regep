package de.saviola.regep.core.gp.function;

import java.lang.reflect.Method;

/**
 * GP-function which wraps an instance method.
 * 
 * TODO rename?
 */
public class InstanceMethodFunction extends MethodFunction
{
  public InstanceMethodFunction(Method method)
  {
    super(method);
  }

  /**
   * Executes the wrapped method.
   * 
   * Expects the first element of the args array to be the object on which the
   * method is to be executed, the rest of the args array will be passed as
   * arguments to the wrapped method.
   */
  @Override
  public Object execute(Object[] args) throws Exception
  {
    // Extract our instance
    Object instance = args[0];
    // Create the new argument array
    Object[] arguments = new Object[args.length - 1];
    System.arraycopy(args, 1, arguments, 0, args.length - 1);

    return this.getMethod().invoke(instance, arguments);
  }

  /**
   * Returns the number of arguments the wrapped method expects +1 for the
   * instance on which the method is to be executed.
   */
  @Override
  public int getArity()
  {
    return this.getMethod().getParameterTypes().length + 1;
  }

  /**
   * Returns the type of the element in the args array expected by
   * {@link #execute(Object[])} at the given index.
   * 
   * @param childNumber 0 – class that declares the wrapped method;
   *  1-n – argument types of the wrapped method.
   */
  @Override
  public Class<?> getChildType(int childNumber)
  {
    return childNumber == 0 ? this.getMethod().getDeclaringClass()
      : this.getMethod().getParameterTypes()[childNumber - 1];
  }
}
