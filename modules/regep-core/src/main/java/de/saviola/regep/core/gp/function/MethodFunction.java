package de.saviola.regep.core.gp.function;

import java.lang.reflect.Method;

import com.esotericsoftware.reflectasm.MethodAccess;

/**
 * GP-function which wraps a method.
 * 
 * Abstract superclass for the classes {@link InstanceMethodFunction} and
 * {@link StaticMethodFunction}.
 */
public abstract class MethodFunction extends AbstractFunction
{
  /**
   * Method index for faster access
   */
  private int accessIndex;

  /**
   * Wrapped method.
   */
  private final Method method;

  /**
   * Method access object.
   */
  private MethodAccess methodAccess;

  public MethodFunction(final Method method)
  {
    this.method = method;
    //this.methodAccess = MethodAccess.get(method.getDeclaringClass());
    //this.accessIndex = this.methodAccess.getIndex(method.getName());
  }

  /**
   * Returns the access index.
   * 
   * Currently not used.
   * 
   * @see MethodAccess#getIndex(String)
   */
  public int getAccessIndex()
  {
    return this.accessIndex;
  }

  /**
   * Returns the wrapped method.
   */
  public Method getMethod()
  {
    return this.method;
  }

  /**
   * Returns the method access.
   *
   * Currently not used.
   * 
   * @see MethodAccess
   */
  public MethodAccess getMethodAccess()
  {
    return this.methodAccess;
  }

  /**
   * Returns the return type of the wrapped method.
   */
  @Override
  public Class<?> getReturnType()
  {
    return this.method.getReturnType();
  }

  /**
   * Returns {@link Method#toString()}.
   */
  @Override
  public String toString()
  {
    return this.method.toString();
  }
}
