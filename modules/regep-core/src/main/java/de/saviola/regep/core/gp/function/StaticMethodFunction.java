package de.saviola.regep.core.gp.function;

import java.lang.reflect.Method;

/**
 * GP-function wrapping a static method.
 */
public class StaticMethodFunction extends MethodFunction
{
  public StaticMethodFunction(Method method)
  {
    super(method);
  }

  /**
   * Invokes the static method with the given arguments.
   */
  @Override
  public Object execute(Object[] args) throws Exception
  {
    return this.getMethod().invoke(null, args);
  }

  /**
   * Returns the number of arguments the wrapped method expects.
   */
  @Override
  public int getArity()
  {
    return this.getMethod().getParameterTypes().length;
  }

  /**
   * Returns the type of the argument to the wrapped method with the given
   * number.
   */
  @Override
  public Class<?> getChildType(int childNumber)
  {
    return this.getMethod().getParameterTypes()[childNumber];
  }
}