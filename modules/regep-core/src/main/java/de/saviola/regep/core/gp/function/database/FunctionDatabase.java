package de.saviola.regep.core.gp.function.database;

import java.util.HashSet;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.saviola.regep.core.gp.function.interfaces.Function;

/**
 * A database of gp functions.
 * 
 * Collects and manages the gp function set.
 */
public class FunctionDatabase<T extends Function>
{
  private static Logger logger = LoggerFactory
    .getLogger(FunctionDatabase.class);

  /**
   * Flag which contains the validation state.
   * 
   * true = revalidate the function set
   * false = function set is still valid
   */
  private boolean dirty = true;

  /**
   * The whole function set, including invalid functions which will not be
   * returned by {@link #getFunctionSet()}.
   */
  private final Set<T> functionSet;

  /**
   * Semi-temporary variables for recursion in {@link #getFunctionSet()}.
   */
  private final Set<T> functionsForbidden = new HashSet<>();

  private final Set<T> functionsInvalid = new HashSet<>();
  private final Set<T> functionsValid = new HashSet<>();
  private final Set<Class<?>> requiredReturnTypes = new HashSet<>();
  /**
   * Root child types for the GP tree.
   * 
   * Will be used to determine which functions should be included in the
   * validated function set ({@link #getFunctionSet()}).
   * 
   * @see #addRootType(Class)
   */
  private final Set<Class<?>> rootTypes;

  public FunctionDatabase()
  {
    this.functionSet = new HashSet<>();
    this.rootTypes = new HashSet<>();

    this.clearRecursionVariables();
  }

  /**
   * Adds the given function to the function set.
   */
  public synchronized void addFunction(final T function)
  {
    this.dirty = true;

    this.functionSet.add(function);
  }

  /**
   * Adds a possible root type.
   * 
   * The root type is the return value of a possible root node in a gp tree.
   * If this function is not called at least once, the function set will always
   * remain empty.
   */
  public void addRootType(final Class<?> type)
  {
    this.clearRecursionVariables();

    this.rootTypes.add(type);
  }

  /**
   * Resets the variables for recursion, signalling {@link #getFunctionSet()}
   * to recreate the set of valid functions.
   */
  private void clearRecursionVariables()
  {
    this.functionsForbidden.clear();
    this.functionsInvalid.clear();
    this.functionsValid.clear();
    this.requiredReturnTypes.clear();
    this.requiredReturnTypes.addAll(this.rootTypes);
  }

  /**
   * Returns the validated function set.
   */
  public Set<T> getFunctionSet()
  {
    // Revalidate the function set if necessary
    if (this.dirty)
    {
      this.validateFunctionSet();
    }

    return this.functionsValid;
  }

  /**
   * Recursively checks the validity of the given function.
   * 
   * A function is valid if for each child type there is a valid function the
   * return type of which equals the child type.
   * 
   * Functions without parameters are always valid.
   * 
   * TODO avoid on-the-fly validation while still searching for functions
   * for other children of a function (e.g., by introducing a flag).
   */
  private boolean isFunctionValid(final T currentFunction)
  {
    boolean valid = this.functionsValid.contains(currentFunction);

    // If it has already been checked
    if (valid || this.functionsInvalid.contains(currentFunction))
    {
      return valid;
    }

    final Set<Class<?>> currentRequiredReturnTypes = new HashSet<>();

    Class<?> childType;
    // Add this function to the set of forbidden functions
    this.functionsForbidden.add(currentFunction);

    // For functions which never enter the following loop
    valid = true;

    // Walk through all parameters
    for (int i = 0; i < currentFunction.getArity(); i++)
    {
      valid = false;
      childType = currentFunction.getChildType(i);

      // Search for a function to satisfy the current parameter type
      for (final T function : this.functionSet)
      {
        // If we found such a function…
        if (function.getReturnType().equals(childType))
        {
          // Skip if it's either forbidden or invalid
          if (this.functionsForbidden.contains(function)
            || this.functionsInvalid.contains(function)
            || !this.isFunctionValid(function))
          {
            continue;
          }

          currentRequiredReturnTypes.add(childType);

          // We found a valid function, break out
          valid = true;
          break;
        }
      }

      // Did we find a valid function for the current child? If not, break out
      if (valid == false)
      {
        break;
      }
    }

    // Remove the current function from the forbidden functions again
    this.functionsForbidden.remove(currentFunction);

    // Add the current function to the valid functions if it's valid
    if (valid == true)
    {
      this.functionsValid.add(currentFunction);
      this.requiredReturnTypes.addAll(currentRequiredReturnTypes);

      logger.debug("Valid function: {}", currentFunction);
    }
    else
    {
      this.functionsInvalid.add(currentFunction);
    }

    return valid;
  }

  /**
   * Validates the function set.
   * 
   * Walks through all functions of the function set and tries to validate them
   * calling {@link #isFunctionValid(Function)}. This is repeated as long as
   * the number of valid function has increased during the current iteration.
   */
  private void validateFunctionSet()
  {
    // Reset recursion variables
    this.clearRecursionVariables();
    int numberOfValidFunctions = 0, numberOfValidFunctionsBefore = 0;

    do
    {
      numberOfValidFunctionsBefore = numberOfValidFunctions;

      for (final T function : this.functionSet)
      {
        // Only check for validity if this function's return type is
        // required by any other valid function or if it's among the
        // root types
        if (this.requiredReturnTypes.contains(function.getReturnType()))
        {
          this.isFunctionValid(function);
        }
      }

      numberOfValidFunctions = this.functionsValid.size();
    }
    // Repeat the process as long as new valid functions have been found
    while (numberOfValidFunctions > numberOfValidFunctionsBefore);

    logger.debug("Statistics: {} valid functions of {} functions overall.",
      new Integer(numberOfValidFunctions),
      new Integer(this.functionSet.size()));

    this.dirty = false;
  }
}
