package de.saviola.regep.core.gp.function.interfaces;

/**
 * Provides a caching mechanism (not only) for executing values of a function.
 */
public interface CachingFunction extends Function
{
  /**
   * Resets the cache.
   */
  public void resetCache();

  /**
   * Sets the cached value for the execution of this function.
   */
  public void setCache(Object value);
}
