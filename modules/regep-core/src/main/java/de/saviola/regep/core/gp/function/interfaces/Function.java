package de.saviola.regep.core.gp.function.interfaces;

/**
 * An interface for GP-functions.
 * 
 * TODO use method terms instead of node terms (arity, child)?
 */
public interface Function
{
  /**
   * Executes the function with the given arguments.
   */
  public Object execute(Object[] args) throws Exception;

  /**
   * Equality check for GP-functions.
   */
  @Override
  public boolean equals(Object obj);

  /**
   * Returns the number of arguments this function takes.
   */
  public int getArity();

  /**
   * Returns the type of the child (argument) with the given number (0-based).
   */
  public Class<?> getChildType(int childNumber);

  /**
   * Returns the return type of this GP-function.
   */
  public Class<?> getReturnType();

  @Override
  public int hashCode();

  /**
   * Returns a string representation of the function.
   */
  @Override
  public String toString();
}