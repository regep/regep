package de.saviola.regep.core.processing;

import java.util.ArrayList;
import java.util.Collection;

import de.saviola.regep.core.processing.exceptions.NotApplicableException;
import de.saviola.regep.core.processing.interfaces.Processor;

/**
 * Abstract processor.
 */
public abstract class AbstractProcessor implements Processor
{
  /**
   * Checks if the processor is applicable for the given input.
   * 
   * This should be the first thing called by subclasses in their
   * {@link #process(Object)} method.
   */
  @Override
  public Collection<?> process(Object input) throws Exception
  {
    if (!this.isApplicableFor(input))
    {
      throw new NotApplicableException();
    }

    return null;
  }

  /**
   * Helper method which wraps an object in a list.
   */
  protected <T> Collection<T> collectionFromObject(T o)
  {
    Collection<T> collection = new ArrayList<>();

    collection.add(o);

    return collection;
  }
}
