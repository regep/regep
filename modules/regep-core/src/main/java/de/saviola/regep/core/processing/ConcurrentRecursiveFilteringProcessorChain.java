package de.saviola.regep.core.processing;

import java.util.Collection;
import java.util.Collections;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

import de.saviola.regep.core.processing.exceptions.NotApplicableException;
import de.saviola.regep.core.processing.interfaces.Processor;

/**
 * Concurrent implementation of the {@link RecursiveFilteringProcessorChain}.
 * 
 * Every input element that is to be processed will be submitted as a task to
 * an {@link ExecutorService}. Additionally, a spawn counter is used to track
 * the overall progress–this counter can only become 0 when no new tasks are
 * going to be created.
 * 
 * Why is that? The spawn counter is incremented before a task to process
 * an input object is submitted and it is decremented only after
 * {@link #processConcurrent(Object)} for this input object has returned. 
 * Due to the recursive nature of the processing, it can be
 * argued that upon returning from {@link #processConcurrent(Object)}, all
 * _direct_ child tasks have already been spawned, thus incrementing the
 * spawn counter (and decrementing it only after having, in turn, spawned
 * all of their direkt child tasks etc.).
 * 
 * The spawn counter will only reach 0 after all tasks have been spawned, not,
 * however, finished. The {@link ExecutorService} will then be
 * {@link ExecutorService#shutdown()} and all remaining tasks can finish.
 */
public class ConcurrentRecursiveFilteringProcessorChain extends
  RecursiveFilteringProcessorChain
{
  /**
   * Pool size for the {@link #threadPool}.
   */
  private int poolSize;

  /**
   * Spawn counter.
   */
  private final AtomicInteger spawnCounter = new AtomicInteger(0);

  /**
   * Thread pool for the concurrent processing.
   * 
   * A new one will be created for each call to {@link #process(Object)}.
   */
  private ExecutorService threadPool;

  /**
   * Creates a new {@link ConcurrentRecursiveFilteringProcessorChain} using
   * the given number of threads.
   */
  public ConcurrentRecursiveFilteringProcessorChain(int numThreads)
  {
    this.poolSize = numThreads;
  }

  /**
   * Decrements the spawn counter and shuts down the thread pool if the
   * counter hit 0.
   */
  private void decrementSpawnCounter()
  {
    int count = this.spawnCounter.decrementAndGet();

    if (count == 0)
    {
      this.threadPool.shutdown();
    }
  }

  /**
   * Increments the spawn counter.
   */
  private void incrementSpawnCounter()
  {
    this.spawnCounter.incrementAndGet();
  }

  /**
   * Spawns a task and submits it to the thread pool.
   */
  private void spawnTask(final Object input)
  {
    // Instantly increment spawn counter. Note that this happens _before_ the
    // task is spawned, thus making it impossible for decrementSpawnCounter()
    // of the parent task to be called before this–it happens on the same
    // thread as the previous call to processConcurrent()
    this.incrementSpawnCounter();

    // Submit the task to the thread pool
    this.threadPool
      .submit(new Callable<Collection<?>>()
      {
        /**
         * Processes the given input object, somewhere, somewhen.
         */
        @Override
        public Collection<?> call() throws Exception
        {
          // Process the current input element
          processConcurrent(input);

          // Decrement the spawn counter after we made sure all tasks resulting
          // from new output objects have been spawned
          decrementSpawnCounter();

          return Collections.emptyList();
        }
      });
  }

  /**
   * Concurrent processing method, will be called from somewhere, somewhen.
   */
  private void processConcurrent(Object input) throws Exception
  {
    // If the object is not accepted by our filter-chain, we can return here
    if (!this.accept(input))
    {
      return;
    }

    Collection<?> outputList;

    // Feed the input into all our processors
    for (Processor processor : this.getProcessors())
    {
      // Reset output list
      outputList = Collections.emptyList();

      // Process the object
      try
      {
        outputList = processor.process(input);
      }
      catch (NotApplicableException e)
      {
        // Nothing to do here
      }
      catch (Exception e)
      {
        // TODO Log + continue?
        e.printStackTrace();
      }

      // Now we walk through all output objects and spawn tasks for them
      for (Object output : outputList)
      {
        this.spawnTask(output);
      }
    }
  }

  /**
   * Entry point for the processing.
   * 
   * TODO collect unprocessed objects and return them?
   */
  @Override
  public Collection<?> process(Object input) throws Exception
  {
    this.threadPool = Executors.newFixedThreadPool(this.poolSize);

    // Spawn the first task
    this.spawnTask(input);

    // At this point we just have to wait for the thread pool to terminate
    // TODO set a time limit?
    this.threadPool.awaitTermination(Long.MAX_VALUE, TimeUnit.NANOSECONDS);

    return Collections.emptyList();
  }
}
