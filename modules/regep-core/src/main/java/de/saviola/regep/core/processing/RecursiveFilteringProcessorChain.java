package de.saviola.regep.core.processing;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.saviola.regep.core.filter.ListFilterChain;
import de.saviola.regep.core.filter.interfaces.Filter;
import de.saviola.regep.core.filter.interfaces.FilterChain;
import de.saviola.regep.core.filter.interfaces.FilterSupport;
import de.saviola.regep.core.processing.exceptions.NotApplicableException;
import de.saviola.regep.core.processing.interfaces.Processor;
import de.saviola.regep.core.processing.interfaces.ProcessorChain;

/**
 * Recursive processor chain implementation which supports filtering.
 * 
 * Each input object is processed by each processor. Each output object will
 * be taken as a new input object.
 * 
 * TODO implement {@link FilterSupport} instead of {@link FilterChain}?
 */
public class RecursiveFilteringProcessorChain implements ProcessorChain,
  FilterChain
{
  private static Logger logger = LoggerFactory
    .getLogger(RecursiveFilteringProcessorChain.class);

  /**
   * To avoid endless recursion, we store the set of classes we came across
   * during resursion in this set.
   */
  private Set<Class<?>> classSet = new HashSet<>();

  /**
   * The filter chain.
   */
  private FilterChain filterChain = new ListFilterChain();

  /**
   * The list of processors.
   */
  private List<Processor> processors = new ArrayList<>();

  /**
   * Returns true if the given object is accepted by the filter chain.
   */
  @Override
  public boolean accept(Object o)
  {
    return this.filterChain.accept(o);
  }

  /**
   * Adds a filter to the filter chain.
   */
  @Override
  public void addFilter(Filter filter)
  {
    this.filterChain.addFilter(filter);
  }

  /**
   * Adds a processor to the processor chain.
   * TODO: processor chain? Better naming? It's not really a chain…
   */
  @Override
  public void addProcessor(Processor processor)
  {
    // Well, this object is a processor, too… Bad idea to add it here, though
    if (processor == this)
    {
      // TODO: throw exception
      return;
    }

    this.processors.add(processor);
  }

  public List<Processor> getProcessors()
  {
    return this.processors;
  }

  /**
   * Returns true if the given object is applicable for at least one of the
   * processors.
   */
  @Override
  public boolean isApplicableFor(Object o)
  {
    for (Processor processor : this.processors)
    {
      if (processor.isApplicableFor(o))
      {
        return true;
      }
    }

    return false;
  }

  /**
   * Recursively processes the given input object.
   * 
   * Every processor will process the given input element (given it is
   * accepted by the filter chain and by
   * {@link Processor#isApplicableFor(Object)} of the processor). The output
   * elements of each processor will be fed back into this method.
   */
  @Override
  public Collection<?> process(Object input) throws Exception
  {
    // If the object is not accepted by our filter-chain, we can return here
    if (!this.accept(input))
    {
      return Collections.emptyList();
    }

    Collection<?> outputList;
    Class<?> outputClass = null;

    // Feed the input into all our processors
    for (Processor processor : this.processors)
    {
      // Reset output list
      outputList = Collections.emptyList();

      // Process the object
      try
      {
        outputList = processor.process(input);
      }
      catch (NotApplicableException e)
      {
        // Nothing to do here
      }
      catch (Exception e)
      {
        // TODO Log + continue?
        e.printStackTrace();
      }

      // Now we walk through all output objects
      for (Object output : outputList)
      {
        // Get the ouput class
        outputClass = output.getClass();

        // If the output class is in the set of disallowed classes, the recursion
        // is aborted and an exception is thrown
        if (this.classSet.contains(outputClass))
        {
          // TODO: throw exception
          logger.error(
            "Possible endless recursion: reoccurring class {}; aborting.",
            outputClass);
          return Collections.emptyList();
        }

        // In the recursion, the current output class must not appear again
        this.classSet.add(outputClass);

        // Recurse into the processing of the output object
        this.process(output);

        // Now that the recursion has returned, the class may appear again
        this.classSet.remove(outputClass);
      }
    }

    return Collections.emptyList();
  }

}
