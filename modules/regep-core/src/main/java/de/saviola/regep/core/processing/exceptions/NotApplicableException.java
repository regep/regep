package de.saviola.regep.core.processing.exceptions;

/**
 * Exception thrown in case a processor is about to process something that he
 * can not.
 */
@SuppressWarnings("serial")
public class NotApplicableException extends Exception
{

}
