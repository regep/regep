package de.saviola.regep.core.processing.interfaces;

import java.util.Collection;

import de.saviola.regep.core.Applicable;

/**
 * An interface for processors.
 * 
 * Processors consist of the {@link #process(Object)} method which takes an
 * input object (this may be null if the processor allows it) and returns a
 * collection of output objects (which may be empty).
 */
public interface Processor extends Applicable
{
  /**
   * Processes the given object returning a collection of output objects.
   * 
   * A processor's {@link Applicable#isApplicableFor(Object)} must return false
   * for each element of the returned collection.
   * TODO enforce this!
   * 
   * @return Collection<?> Output of the process, may be empty.
   */
  public Collection<?> process(Object input) throws Exception;
}
