package de.saviola.regep.core.processing.interfaces;

/**
 * Interface to allow processor-chaining.
 */
public interface ProcessorChain extends Processor
{
  /**
   * Adds a new processor to the chain.
   */
  public void addProcessor(Processor processor);
}
