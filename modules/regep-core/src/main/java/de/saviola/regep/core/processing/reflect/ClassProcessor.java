package de.saviola.regep.core.processing.reflect;

import de.saviola.regep.core.processing.AbstractProcessor;

/**
 * Abstract processor for class objects.
 */
public abstract class ClassProcessor extends AbstractProcessor
{
  /**
   * Returns true if the given input is a class object.
   */
  @Override
  public boolean isApplicableFor(Object input)
  {
    return (input instanceof Class);
  }
}
