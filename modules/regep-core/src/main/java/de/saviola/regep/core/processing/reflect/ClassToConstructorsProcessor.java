package de.saviola.regep.core.processing.reflect;

import java.util.Arrays;
import java.util.Collection;

/**
 * Processor that extracts constructors from a class.
 */
public class ClassToConstructorsProcessor extends ClassProcessor
{
  /**
   * Returns a list of constructors declared by the given class.
   */
  @Override
  public Collection<?> process(Object input) throws Exception
  {
    super.process(input);

    return Arrays.asList(((Class<?>) input).getConstructors());
  }
}
