package de.saviola.regep.core.processing.reflect;

import java.lang.reflect.Modifier;
import java.util.Collection;

import org.reflections.ReflectionUtils;

/**
 * Processor that extracts methods from classes.
 */
public class ClassToMethodsProcessor extends ClassProcessor
{
  /**
   * Returns a set of methods defined by the given class.
   */
  @SuppressWarnings("unchecked")
  @Override
  public Collection<?> process(Object input) throws Exception
  {
    super.process(input);

    return ReflectionUtils.getAllMethods((Class<?>) input,
      ReflectionUtils.withModifier(Modifier.PUBLIC));
  }
}
