package de.saviola.regep.core.processing.reflect;

import java.lang.reflect.Constructor;
import java.util.Collection;

import de.saviola.regep.core.gp.function.ConstructorFunction;
import de.saviola.regep.core.processing.AbstractProcessor;

/**
 * Processor which wraps {@link Constructor} objects in
 * {@link ConstructorFunction} objects.
 */
public class ConstructorProcessor extends AbstractProcessor
{
  /**
   * Returns true if the given object is a constructor. 
   */
  @Override
  public boolean isApplicableFor(Object input)
  {
    return (input instanceof Constructor<?>);
  }

  /**
   * Wraps the given constructor in a {@link ConstructorFunction} and returns
   * it.
   */
  @Override
  public Collection<?> process(Object input) throws Exception
  {
    super.process(input);

    Constructor<?> constructor = (Constructor<?>) input;

    return this.collectionFromObject(new ConstructorFunction(constructor));
  }
}
