package de.saviola.regep.core.processing.reflect;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.Collection;

import de.saviola.regep.core.gp.function.InstanceMethodFunction;

/**
 * Processor which wraps instance methods in {@link InstanceMethodFunction}
 * objects.
 */
public class InstanceMethodProcessor extends MethodProcessor
{
  /**
   * Returns true if the given object is an instance method.
   */
  @Override
  public boolean isApplicableFor(Object input)
  {
    return super.isApplicableFor(input)
      && !Modifier.isStatic(((Method) input).getModifiers());
  }

  /**
   * Wraps the given instance method in an {@link InstanceMethodFunction} and
   * returns it.
   */
  @Override
  public Collection<?> process(Object input) throws Exception
  {
    super.process(input);

    Method method = (Method) input;

    return this.collectionFromObject(new InstanceMethodFunction(method));
  }
}
