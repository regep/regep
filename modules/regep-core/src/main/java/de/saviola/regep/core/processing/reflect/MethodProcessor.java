package de.saviola.regep.core.processing.reflect;

import java.lang.reflect.Method;

import de.saviola.regep.core.processing.AbstractProcessor;

/**
 * Abstract processor of Methods.
 */
public abstract class MethodProcessor extends AbstractProcessor
{
  /**
   * Returns true if the given object is a {@link Method} instance.
   */
  @Override
  public boolean isApplicableFor(Object input)
  {
    return (input instanceof Method);
  }
}