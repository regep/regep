package de.saviola.regep.core.processing.reflect;

import java.util.Collection;

import org.reflections.Reflections;

import de.saviola.regep.core.processing.AbstractProcessor;

/**
 * Processor of packages.
 */
public class PackageProcessor extends AbstractProcessor
{
  /**
   * Returns true of the given object is a {@link String}.
   */
  @Override
  public boolean isApplicableFor(Object input)
  {
    return (input instanceof String);
  }

  /**
   * Returns all classes defined in the given package.
   */
  @Override
  public Collection<?> process(Object input) throws Exception
  {
    super.process(input);

    Reflections reflections = new Reflections(input);

    return reflections.getSubTypesOf(Object.class);
  }
}
