package de.saviola.regep.core.processing.reflect;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.Collection;

import de.saviola.regep.core.gp.function.StaticMethodFunction;

/**
 * A processor of static methods.
 */
public class StaticMethodProcessor extends MethodProcessor
{
  /**
   * Returns true if the given object is a static method.
   */
  @Override
  public boolean isApplicableFor(Object input)
  {
    return super.isApplicableFor(input)
      && Modifier.isStatic(((Method) input).getModifiers());
  }

  /**
   * Wraps the static method in a {@link StaticMethodFunction} and returns it.
   */
  @Override
  public Collection<?> process(Object input) throws Exception
  {
    super.process(input);

    Method method = (Method) input;

    return this.collectionFromObject(new StaticMethodFunction(method));
  }
}
