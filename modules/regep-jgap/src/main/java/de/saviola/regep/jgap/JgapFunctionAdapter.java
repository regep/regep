package de.saviola.regep.jgap;

import org.jgap.InvalidConfigurationException;
import org.jgap.gp.CommandGene;
import org.jgap.gp.IGPProgram;
import org.jgap.gp.IMutateable;
import org.jgap.gp.impl.GPConfiguration;
import org.jgap.gp.impl.ProgramChromosome;
import org.jgap.util.ICloneable;

import de.saviola.regep.core.gp.function.CachingFunctionWrapper;
import de.saviola.regep.core.gp.function.interfaces.Function;

/**
 * Function wrapper, acting as an adapter between {@link CommandGene} and
 * {@link Function}.
 */
@SuppressWarnings("serial")
public class JgapFunctionAdapter extends CommandGene implements ICloneable,
  IMutateable, Function
{
  /**
   * The wrapped function.
   */
  private final CachingFunctionWrapper functionWrapper;

  public JgapFunctionAdapter(final GPConfiguration conf,
    final CachingFunctionWrapper function)
    throws Exception
  {
    // Here we call the super constructor, passing through the configuration,
    // as well as the arity and return type which we extract from the given
    // method
    super(conf, function.getArity(), function.getReturnType());

    this.functionWrapper = function;

    // The method is here used as application data and will hence be used when
    // comparing this function gene to others
    this.setApplicationData(this.functionWrapper);
    this.setCompareApplicationData(true);
  }

  /**
   * Applies mutation, using {@link CachingFunctionWrapper#resetCache()}.
   */
  @Override
  public CommandGene applyMutation(final int index, final double percentage)
    throws InvalidConfigurationException
  {
    // This effectively only changes something, if the underlying functionWrapper
    // returns something different with every call.
    // TODO make percentage configurable?
    if (percentage > .75)
    {
      this.functionWrapper.resetCache();
    }

    return this;
  }

  /**
   * Clone the function wrapper.
   */
  @Override
  public JgapFunctionAdapter clone()
  {
    JgapFunctionAdapter clone;
    try
    {
      clone = new JgapFunctionAdapter(this.getGPConfiguration(),
        new CachingFunctionWrapper(this.functionWrapper.getFunction()));
      clone.setReturnType(this.getReturnType());

      return clone;
    }
    catch (final Exception e)
    {
      e.printStackTrace();

      // TODO handle sitation?
      throw new RuntimeException("Failed to clone JgapFunctionAdapter.");
    }

  }

  /**
   * Calls execute on the underlying function.
   */
  @Override
  public Object execute(final Object[] args) throws Exception
  {
    return this.functionWrapper.execute(args);
  }

  /**
   * Executes this node.
   */
  @Override
  public Object execute(final ProgramChromosome c, final int n,
    final Object[] args)
  {
    try
    {
      return this.functionWrapper.execute(args);
    }
    catch (final Exception e)
    {
      // TODO individual exception
      throw new RuntimeException("Error while executing node.");
    }
  }

  /**
   * Executes this node and returns an object.
   */
  @Override
  public Object execute_object(final ProgramChromosome c, final int n,
    final Object[] args)
  {
    return this.execute(c, n, this.executeChildren(c, n, args));
  }

  /**
   * Executes child nodes.
   */
  private Object[] executeChildren(final ProgramChromosome c, final int n,
    final Object[] args)
  {
    // One output value for each child
    final Object[] output = new Object[this.functionWrapper.getArity()];

    // Walk through all children
    for (int i = 0; i < output.length; i++)
    {
      output[i] = c.execute_object(n, i, args);
    }

    return output;
  }

  /**
   * Returns the arity of the function.
   */
  @Override
  public int getArity()
  {
    return this.functionWrapper.getArity();
  }

  /**
   * Returns the type of the child with the given number.
   */
  @Override
  public Class<?> getChildType(final IGPProgram a_ind, final int a_chromNum)
  {
    return this.getChildType(a_chromNum);
  }

  /**
   * Returns the child type of the function.
   */
  @Override
  public Class<?> getChildType(final int childNumber)
  {
    return this.functionWrapper.getChildType(childNumber);
  }

  /**
   * Returns the name of the function.
   */
  @Override
  public String getName()
  {
    return this.functionWrapper.toString();
  }

  /**
   * Returns the return type of the function.
   * 
   * The return type might have been explicitly set via
   * {@link CommandGene#setReturnType(Class)} and differ from
   * {@link Function#getReturnType()}, which is why we call
   * {@link CommandGene#getReturnType()} here.
   */
  @Override
  public Class<?> getReturnType()
  {
    return super.getReturnType();
  }

  @Override
  public int hashCode()
  {
    return this.functionWrapper.hashCode();
  }

  /**
   * Returns a string representation of the wrapped function, i.e.
   * {@link Function#toString()}.
   */
  @Override
  public String toString()
  {
    return "[" + this.getReturnType() + "]" + this.functionWrapper.toString();
  }
}
