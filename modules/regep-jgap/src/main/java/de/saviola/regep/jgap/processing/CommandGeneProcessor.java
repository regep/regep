package de.saviola.regep.jgap.processing;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

import org.jgap.gp.CommandGene;

import de.saviola.regep.core.gp.function.database.FunctionDatabase;
import de.saviola.regep.core.processing.AbstractProcessor;
import de.saviola.regep.jgap.JgapFunctionAdapter;

/**
 * Processor of {@link JgapFunctionAdapter} objects.
 * 
 * This processor simply collects all {@link JgapFunctionAdapter} it comes
 * across and saves them in a {@link FunctionDatabase}.
 */
public class CommandGeneProcessor extends AbstractProcessor
{
  /**
   * Collection of command genes.
   */
  private FunctionDatabase<JgapFunctionAdapter> functionDatabase =
    new FunctionDatabase<>();

  /**
   * Converts the function set of the database to a collection of command
   * genes and returns it.
   */
  public Collection<CommandGene> getCommandGenes()
  {
    Collection<? extends CommandGene> functionSet =
      this.functionDatabase.getFunctionSet();
    Collection<CommandGene> commandGenes = new ArrayList<>(functionSet.size());

    for (Object function : functionSet)
    {
      commandGenes.add((CommandGene) function);
    }

    return commandGenes;
  }

  public FunctionDatabase<JgapFunctionAdapter> getFunctionDatabase()
  {
    return this.functionDatabase;
  }

  /**
   * Returns true if the given object is a {@link CommandGene} instance.
   */
  @Override
  public boolean isApplicableFor(Object input)
  {
    return (input instanceof JgapFunctionAdapter);
  }

  /**
   * Adds the given {@link CommandGene} to the internal collection.
   */
  @Override
  public Collection<?> process(Object input) throws Exception
  {
    super.process(input);

    this.functionDatabase.addFunction((JgapFunctionAdapter) input);

    // No output
    return Collections.emptyList();
  }
}
