package de.saviola.regep.jgap.processing;

import java.util.ArrayList;
import java.util.Collection;

import org.jgap.gp.CommandGene;
import org.jgap.gp.impl.GPConfiguration;

import de.saviola.regep.core.gp.function.CachingFunctionWrapper;
import de.saviola.regep.core.gp.function.interfaces.Function;
import de.saviola.regep.core.processing.AbstractProcessor;
import de.saviola.regep.jgap.JgapFunctionAdapter;

/**
 * Processor of {@link Function} instances.
 */
public class FunctionProcessor extends AbstractProcessor
{
  /**
   * Jgap configuration object, necessary for create {@link JgapFunctionAdapter}
   * objects.
   */
  private GPConfiguration configuration;

  public FunctionProcessor(GPConfiguration configuration)
  {
    this.configuration = configuration;
  }

  /**
   * Returns true if the given object is a {@link Function} instance but not an
   * instance of {@link JgapFunctionAdapter}.
   */
  @Override
  public boolean isApplicableFor(Object input)
  {
    return (input instanceof Function)
      && !(input instanceof JgapFunctionAdapter);
  }

  /**
   * Wraps the given {@link Function} in a {@link JgapFunctionAdapter} which
   * extends {@link CommandGene}, allowing our functions to be used with Jgap.
   * 
   * For each function's return value, we will walk up the class-hierarchy and
   * register a new function for each type encountered on the way, to make
   * type matching more dynamic and flexible.
   */
  @Override
  public Collection<?> process(Object input) throws Exception
  {
    super.process(input);

    // Wrap the function in a caching function wrapper
    CachingFunctionWrapper function =
      new CachingFunctionWrapper((Function) input);

    // Collection to be returned
    Collection<JgapFunctionAdapter> wrappers = new ArrayList<>();

    // Loop variables
    JgapFunctionAdapter currentWrapper;
    Class<?> returnType = function.getReturnType();

    // Object.class.getSuperclass() returns null, stop at that point
    while (returnType != null)
    {
      // Create wrapper and explicitly set return type
      // (Otherwise return type of function would be used)
      currentWrapper = new JgapFunctionAdapter(this.configuration, function);
      currentWrapper.setReturnType(returnType);

      wrappers.add(currentWrapper);

      // Walk up the class hierarchy
      returnType = returnType.getSuperclass();
    }

    return wrappers;
  }
}
